/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      animation: {
        appear: "appear 2s ease-in",
        marka: "marka 0.5s ease-in",
        wiggle: "wiggle 2s ease-in-out 5s",
      },
      keyframes: {
        appear: {
          "0%": {
            opacity: "0",
          },
          "25%": {
            opacity: "0",
          },
          "50%": {
            opacity: "0",
          },
          "75%": {
            opacity: "1",
          },
          "100%": {
            opacity: "1",
          },
        },
        marka: {
          "0%": {
            opacity: "0",
          },
          "100%": {
            opacity: ".6",
          },
        },
        wiggle: {
          "0%": { transform: "rotate(0deg)" },
          "60%": { transform: "rotate(10deg)" },
          "70%": { transform: "rotate(-10deg)" },
          "80%": { transform: "rotate(20deg)" },
          "90%": { transform: "rotate(-5deg)" },
          "100%": { transform: "rotate(0deg)" },
        },
      },
    },
  },
  plugins: [require("tailwindcss"), require("autoprefixer")],
};
